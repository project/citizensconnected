api = 2
core = 7.x

includes[] = drupal-org-core.make

projects[citizensconnected][type] = profile
projects[citizensconnected][download][type] = git
projects[citizensconnected][download][url] = https://git.drupal.org/project/citizensconnected.git
projects[citizensconnected][download][branch] = 7.x-1.x
